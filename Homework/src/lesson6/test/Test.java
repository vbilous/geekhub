package lesson6.test;

import lesson6.json.JsonSerializer;
import lesson6.json.adapters.DateAdapter;
import org.json.JSONException;
import org.json.JSONObject;

import java.awt.*;
import java.lang.reflect.Field;
import java.util.*;
import java.util.List;

public class Test {
    public static void main(String[] args) throws IllegalAccessException, JSONException {
        Cat cat = new Cat();
        cat.setColor(Color.GRAY);
        cat.setAge(4);
        cat.setName("Tom");

        /**
         * My examples
         */
//        Class c = cat.getClass();
//        Field[] fields = c.getDeclaredFields();
//        for (Field field : fields) {
//            Class fieldType = field.getType();
//            System.out.println("Name: " + field.getName());
//            System.out.println("Type: " + fieldType.getName());
//        }
//        try {
//            Field field = c.getDeclaredField("age");
//            field.setAccessible(true);
//            System.out.println(field);
//            int nameValue = (int) field.get(cat);
//            System.out.println("value= " + nameValue);
//        } catch (NoSuchFieldException e) {
//            System.out.println("error");
//        }

        List<Integer> whiskers = cat.getWhiskers();
        whiskers.add(1);
        whiskers.add(2);
        whiskers.add(3);
        whiskers.add(4);
        whiskers.add(5);
        whiskers.add(6);

        cat.setBirthDate(new Date());
        cat.getPaws().put("front-left", new Paw(23, Color.GRAY));
        cat.getPaws().put("front-right", new Paw(24, Color.WHITE));
        cat.getPaws().put("back-left", new Paw(23, Color.BLACK));
        cat.getPaws().put("back-right", new Paw(22, Color.GRAY));

        JSONObject o = (JSONObject) JsonSerializer.serialize(cat);
        System.out.println(o.toString(4));
    }
}
