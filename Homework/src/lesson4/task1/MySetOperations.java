package lesson4.task1;

import java.util.HashSet;
import java.util.Set;

public class MySetOperations implements SetOperations {
    @Override
    public boolean equals(Set a, Set b) {
        return a.equals((Set) b) ? true : false;
    }

    @Override
    public Set union(Set a, Set b) {
        Set temporarySet = new HashSet();
        temporarySet.addAll(a);
        temporarySet.addAll(b);
        return temporarySet;
    }

    @Override
    public Set subtract(Set a, Set b) {
        Set temporarySet = new HashSet();
        boolean equal;
        for (Object i : a) {
            equal = true;
            for (Object j : b) {
                if (i.equals(j)) {
                    equal = false;
                }
            }
            if (equal) {
                temporarySet.add(i);
            }
        }
        return temporarySet;
    }

    @Override
    public Set intersect(Set a, Set b) {
        Set temporarySet = new HashSet();
        for (Object i : a) {
            for (Object j : b) {
                if (i.equals(j)) {
                    temporarySet.add(i);
                }
            }
        }
        return temporarySet;
    }

    @Override
    public Set symmetricSubtract(Set a, Set b) {
        return union(subtract(a, b), subtract(b, a));
    }
}
