package lesson5;

import lesson5.source.SourceLoader;
import lesson5.source.URLSourceProvider;

import java.io.IOException;
import java.util.Scanner;

public class TranslatorController {

    public static void main(String[] args) throws IOException {
        //initialization
        SourceLoader sourceLoader = new SourceLoader();
        Translator translator = new Translator(new URLSourceProvider());

        Scanner scanner = new Scanner(System.in);
        String command = inputPath(scanner);
        while (!"exit".equals(command)) {
            try {
                String source = sourceLoader.loadSource(command);
                String translation = translator.translate(source);

                System.out.println("Result\n Original: \n" + source);
                System.out.println(" Translation: \n" + translation);
            } catch (IOException e) {
                System.out.println("CRITICAL ERROR!");
            }
            command = inputPath(scanner);
        }
    }

    private static String inputPath(Scanner scanner) {
        String command;
        System.out.print("\nPlease enter path to text you want to translate, or 'exit' if you want exit\n" +
                "(For example: 'https://dl.dropboxusercontent.com/u/14434019/en.txt' or 'e:/1.txt'): ");
        command = scanner.next();
        return command;
    }
}

