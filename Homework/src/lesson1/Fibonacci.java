package  lesson1;

import java.util.Scanner;

public class Fibonacci {
    public static void main(String[] args) {
        System.out.print("Введіть N: ");
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int backOne = 1, backTwo = 0, fibonacci = 1;
        System.out.print("Послідовність Фібоначчі:");
        for (int i = 1; i <= n; i++) {
            fibonacci = backOne + backTwo;
            System.out.print(" " + fibonacci);
            backOne = backTwo;
            backTwo = fibonacci;
        }
    }
}

