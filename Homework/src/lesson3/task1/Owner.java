package lesson3.task1;

public class Owner implements Comparable {
    private int age;
    private String name;

    public void setAge(int age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    @Override
    public int compareTo(Object o) {
        Owner anotherPerson = (Owner) o;
        if (anotherPerson.age != this.age) {
            if (this.age < anotherPerson.age) {
                return -1;
            } else {
                return 1;
            }
        }
        return 0;
    }
}
