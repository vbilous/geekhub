package lesson3.task1;

public class Car implements Comparable {
    private int speed;
    private int weight;
    private String name;

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSpeed() {
        return speed;
    }

    public int getWeight() {
        return weight;
    }

    public String getName() {
        return name;
    }

    Car(int speed, int weight){
        this.speed = speed;
        this.weight = weight;
    }

    @Override
    public int compareTo(Object o) {
        Car anotherPerson = (Car) o;
        if (anotherPerson.speed != this.speed) {
            if (this.speed < anotherPerson.speed) {
                return -1;
            } else {
                return 1;
            }
        }
        return 0;
    }
}
