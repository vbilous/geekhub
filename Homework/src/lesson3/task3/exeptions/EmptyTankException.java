package lesson3.task3.exeptions;

public class EmptyTankException extends Exception {

    public EmptyTankException(String message) {
        super(message);
    }

    public EmptyTankException() {
    }
}
