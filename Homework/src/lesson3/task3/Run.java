package lesson3.task3;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Run {
    private static String selectedVehicle;
    private static Vehicle vehicle;
    private static Scanner input;
    private static int balanceFuel, maxFuel, maxSpeed, fuelConsumption;

    public static void main(String[] args) {
        chooseVehicle();
        while (true) {
            actionWithVehicle(chooseAction());
        }
    }

    private static int chooseAction() {
        try{
        System.out.print("\n1 - Drive 100 km\n2 - Raise the speed to 10 km\n3 - Reduce the speed to 10 km\n" +
                "4 - Add 10 liters of fuel\n5 - Turn\n6 - Change vehicle\n7 - exit\n Choose action: ");
        input = new Scanner(System.in);
        return input.nextInt();
        } catch (NumberFormatException e) {
            System.out.println("Input data incorrect!");
            chooseAction();
            return 0;
        } catch (InputMismatchException e){
            System.out.println("Input data incorrect!");
            chooseAction();
            return 0;
        }
    }

    private static void actionWithVehicle(int actionNumber) {
        switch (actionNumber) {
            case 1:
                vehicle.drive(100);
                showCurrentState();
                break;
            case 2:
                vehicle.accelerate(10);
                showCurrentState();
                break;
            case 3:
                vehicle.brake(10);
                showCurrentState();
                break;
            case 4:
                vehicle.addFuel(10);
                showCurrentState();
                break;
            case 5:
                System.out.println("   1 - right");
                System.out.println("  -1 - left");
                System.out.println("   0 - straight");
                System.out.print("    Enter the direction you want to rotate: ");
                input = new Scanner(System.in);
                vehicle.turn(input.nextInt());
                showCurrentState();
                break;
            case 6:
                chooseVehicle();
                break;
            case 7:
                System.exit(0);
        }
    }

    private static void showCurrentState() {
        System.out.print("   Your current fuel (charge) - " + vehicle.getFuel());
        System.out.print("\n   Your speed - " + vehicle.getSpeed());
        System.out.print("\n   Your passed way - " + vehicle.getPassedWay());
        System.out.print("\n   Your fuel consumption - " + vehicle.getFuelConsumption());
        switch (vehicle.getDirection()) {
            case 1:
                System.out.print("\n   Your direction - right");
                break;
            case -1:
                System.out.print("\n   Your direction - left");
                break;
            case 0:
                System.out.print("\n   Your direction - straight");
                break;
        }
        if (vehicle.getEngineInAction()) {
            System.out.print("\n   Your engine in action");
        } else {
            System.out.print("\n   Your engine is stopped");
        }
    }


    private static void chooseVehicle() {
        System.out.print("\n 1 - Car\n 2 - SolarPowerCar\n 3 - Boat \n Choose vehicle: ");
        input = new Scanner(System.in);
        selectedVehicle = input.nextLine();

        try {
            switch (Integer.parseInt(selectedVehicle)) {
                case 1: {
                    enterInformationVehicle();
                    vehicle = new Car(balanceFuel, maxFuel, maxSpeed, fuelConsumption);
                    break;
                }
                case 2: {
                    enterInformationVehicle();
                    vehicle = new SolarPoweredCar(balanceFuel, maxFuel, maxSpeed, fuelConsumption);
                    break;
                }
                case 3: {
                    enterInformationVehicle();
                    vehicle = new Boat(balanceFuel, maxFuel, maxSpeed, fuelConsumption);
                    break;
                }
            }
        } catch (NumberFormatException e) {
            System.out.println("  Input data incorrect!");
            chooseVehicle();
        } catch (InputMismatchException e){
            System.out.println("  Input data incorrect!");
            actionWithVehicle(chooseAction());
        }

        showCurrentState();
    }

    private static void enterInformationVehicle() {
        System.out.print("  Enter max size of the fuel tank (battery): ");
        input = new Scanner(System.in);
        maxFuel = input.nextInt();

        System.out.print("  Enter current amount of fuel (charge): ");
        input = new Scanner(System.in);
        balanceFuel = input.nextInt();

        System.out.print("  Enter maximum speed: ");
        input = new Scanner(System.in);
        maxSpeed = input.nextInt();

        System.out.print("  Enter fuel (charge) consumption per 100 km: ");
        input = new Scanner(System.in);
        fuelConsumption = input.nextInt();
    }
}