package lesson3.task3;

/**
 * Created with IntelliJ IDEA.
 * User: Администратор
 * Date: 22.10.13
 * Time: 2:01
 * To change this template use File | Settings | File Templates.
 */
public class ElectricEngine implements ForceProvider {
    private boolean engineInAction;
    private float fuelConsumption;

    @Override
    public float getFuelConsumption() {
        return fuelConsumption;
    }

    @Override
    public boolean getEngineInAction() {
        return engineInAction;
    }

    @Override
    public void setEngineInAction(boolean engineInAction) {
        this.engineInAction = engineInAction;
    }

    ElectricEngine(boolean engineInAction, float fuelConsumption){
        this.engineInAction = engineInAction;
        this.fuelConsumption = fuelConsumption;
    }
}
