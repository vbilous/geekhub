package lesson3.task3;

import lesson3.task3.exeptions.EngineStoppedException;
import lesson3.task3.exeptions.OverspeedException;

public class Wheel implements ForceAcceptor {
    private int speed;
    private int maxSpeed;
    private int direction; //1 - right; -1 - left; 0 - straight
    private int passedWay;

    Wheel(int maxSpeed) {
        this.maxSpeed = maxSpeed;
        this.speed = 0;
        this.direction = 0;
        this.passedWay = 0;
    }

    @Override
    public void setPassedWay(int passedWay) {
        this.passedWay += passedWay;
    }

    @Override
    public int getPassedWay() {
        return passedWay;
    }

    @Override
    public void setSpeed(int speed) {
        this.speed = speed;
    }

    @Override
    public int getDirection() {
        return direction;
    }

    @Override
    public void setDirection(int direction) {
        if (direction >= 1) {
            this.direction = 1;
        } else if (direction <= -1) {
            this.direction = direction;
        } else {
            this.direction = direction;
        }
    }

    @Override
    public int getSpeed() {
        return speed;
    }

    @Override
    public void addSpeed(int accelerate) {
        try {
            if ((accelerate + this.speed) > this.maxSpeed) {
                this.speed = this.maxSpeed;
                throw new OverspeedException();
            } else if (accelerate >= 0) {
                this.speed += accelerate;
            }
        } catch (OverspeedException e) {
            System.out.println("ERROR! Your car cant raise speed more than " + maxSpeed + " km!\n");
        }
    }

    public void reduceSpeed(int reduce) {
        try {
            if ((this.speed - reduce) < 0) {
                this.speed = 0;
                throw new EngineStoppedException();
            } else if (reduce >= 0) {
                this.speed -= reduce;
            }
        } catch (EngineStoppedException e) {
            System.out.println("Your engine is stopped!\n");
        }
    }
}
