package lesson3.task3;

import lesson3.task3.exeptions.EmptyTankException;


public class GasTank implements EnergyProvider {
    private float fuel;
    private int maxFuel;

    GasTank(int currentFuel, int maxFuel) {
        this.maxFuel = maxFuel;
        if (currentFuel >= maxFuel) {
            this.fuel = maxFuel;
        } else {
            this.fuel = currentFuel;
        }

    }

    @Override
    public int getMaxFuel() {
        return maxFuel;
    }

    @Override
    public float getFuel() {
        return fuel;
    }

    @Override
    public void addFuel(float fuel) {

        if (fuel + this.fuel <= this.maxFuel) {
            this.fuel += fuel;
            //має бути ексепшин
        }
    }

    @Override
    public void burnFuel(float amountFuel) {
        try {
            if ((amountFuel >= 0) && (amountFuel <= fuel)) {
                this.fuel -= amountFuel;
            } else if (amountFuel > this.fuel) {
                throw new EmptyTankException();
            }
        } catch (EmptyTankException e) {
            System.out.println("ERROR! Not enough gasoline! Add fuel!\n");
        }
    }
}
