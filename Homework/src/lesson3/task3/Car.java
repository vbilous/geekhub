package lesson3.task3;

public class Car extends Vehicle {
    private Wheel wheel;
    private GasTank gasTank;
    private PetrolEngine petrolEngine;

    Car(int balanceFuel, int maxFuel, int maxSpeed, float fuelConsumption) {
        this.wheel = new Wheel(maxSpeed);
        this.gasTank = new GasTank(balanceFuel, maxFuel);
        this.petrolEngine = new PetrolEngine(false, fuelConsumption);
    }

    @Override
    public int getPassedWay() {
        return wheel.getPassedWay();
    }

    @Override
    public void setPassedWay(int passedWay) {
        wheel.setPassedWay(passedWay);    //To change body of overridden methods use File | Settings | File Templates.
    }

    @Override
    public void drive(int passedWay){
        if(petrolEngine.getEngineInAction()){
            gasTank.burnFuel(petrolEngine.getFuelConsumption());
            wheel.setPassedWay(passedWay);
        }
        else{
            System.out.println("RAISE YOUR SPEED! Engine stopped!\n");
        }
    }

    @Override
    public float getFuel() {
        return gasTank.getFuel();
    }

    @Override
    public float getFuelConsumption() {
        return petrolEngine.getFuelConsumption();
    }

    @Override
    public void addFuel(float fuel) {
        gasTank.addFuel(fuel);
    }
    @Override
    public int getDirection() {
        return wheel.getDirection();
    }
    @Override
    public int getSpeed() {
        return wheel.getSpeed();
    }
    @Override
    public boolean getEngineInAction() {
        return petrolEngine.getEngineInAction();
    }

    @Override
    public void accelerate(int accelerateSpeed) {
        if (gasTank.getFuel() == 0) {
            wheel.setSpeed(0);
            petrolEngine.setEngineInAction(false);
        } else {
            wheel.addSpeed(accelerateSpeed);
            petrolEngine.setEngineInAction(true);
        }
    }

    @Override
    public void brake(int reduceSpeed) {
        wheel.reduceSpeed(reduceSpeed);
        if (wheel.getSpeed() == 0) {
            petrolEngine.setEngineInAction(false);
            wheel.setDirection(0);
        }
    }

    @Override
    public void turn(int direction) {
        wheel.setDirection(direction);
        wheel.reduceSpeed(1);//when the actionWithCar turns, its speed decreases.
    }
}
