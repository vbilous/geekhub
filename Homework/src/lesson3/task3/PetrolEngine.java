package lesson3.task3;

public class PetrolEngine implements ForceProvider {
    private boolean engineInAction;
    private float fuelConsumption;

    @Override
    public float getFuelConsumption() {
        return fuelConsumption;
    }

    @Override
    public boolean getEngineInAction() {
        return engineInAction;
    }

    @Override
    public void setEngineInAction(boolean engineInAction) {
        this.engineInAction = engineInAction;
    }

    PetrolEngine(boolean engineInAction, float fuelConsumption){
        this.engineInAction = engineInAction;
        this.fuelConsumption = fuelConsumption;
    }
}
