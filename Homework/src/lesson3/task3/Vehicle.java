package lesson3.task3;

public abstract class Vehicle implements Driveable {
    public int getPassedWay(){
        return 0;
    }

    public void setPassedWay(int passedWay){
    }

    public void drive(int passedWay){
    }

    public float getFuel(){
        return 0;
    }

    public void addFuel(float fuel){
    }

    public float getFuelConsumption() {
        return 0;
    }

    public int getDirection() {
        return 0;
    }

    public void setDirection(int direction) {
    }

    public int getSpeed() {
        return 0;
    }

    public void addSpeed(int speed) {
    }

    public void reduceSpeed(int reduceSpeed){
    }

    public boolean getEngineInAction() {
        return false;
    }

    @Override
    public void accelerate(int accelerateSpeed) {

    }

    @Override
    public void brake(int reduceSpeed) {

    }

    @Override
    public void turn(int direction) {

    }
}

