package lesson3.task3;

public interface EnergyProvider {
    public int getMaxFuel();

    public float getFuel();

    public void addFuel(float fuel);

    public void burnFuel(float amountFuel);
}
