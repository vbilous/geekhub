package lesson3.task3;

public interface ForceAcceptor {

    public void setSpeed(int speed);

    public int getDirection();

    public void setDirection(int direction);

    public int getSpeed();

    public void addSpeed(int accelerate);

    public void reduceSpeed(int reduce);

    public int getPassedWay();

    public void setPassedWay(int passedWay);

}
