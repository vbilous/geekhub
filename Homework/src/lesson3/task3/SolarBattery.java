package lesson3.task3;

import lesson3.task3.exeptions.EmptyTankException;


public class SolarBattery implements EnergyProvider {
    private float fuel;
    private int maxFuel;

    SolarBattery(int fuel, int maxBatteryCharge) {
        this.maxFuel = maxBatteryCharge;
        if (fuel >= maxBatteryCharge) {
            this.fuel = maxBatteryCharge;
        } else {
            this.fuel = fuel;
        }
    }

    @Override
    public float getFuel() {
        return fuel;
    }

    @Override
    public int getMaxFuel() {
        return maxFuel;
    }

    @Override
    public void addFuel(float fuel) {

            if (fuel + this.fuel <= this.maxFuel) {
                this.fuel += fuel;
                //має бути ексепшин
            }

    }

    @Override
    public void burnFuel(float amountFuel) {
        try {
            if ((amountFuel >= 0) && (amountFuel <= fuel)) {
                this.fuel -= amountFuel;
            } else if (amountFuel > this.fuel) {
                throw new EmptyTankException();
            }
        } catch (EmptyTankException e) {
            System.out.println("ERROR! Not enough gasoline! Add charge!\n");
        }
    }
}

