package lesson3.task2;


import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input;
        while (true) {
            testString();
            testStringBuilder();
            testStringBuffer();
            System.out.println("\nTest again?\n 1 - yes\n Any key - Exit");
            System.out.print("Your choose: ");
            input = new Scanner(System.in);
            switch (input.nextInt()){
                case 1:{
                    continue;
                }
                default:{
                    System.exit(0);
                }
            }
        }
    }

    private static void testString() {
        long startTime = 0, endTime;
        String string = new String();
        System.out.println("\nSting:");
        for (int i = 0; i < 10000; i++) {
            if (i == 0) {
                startTime = System.currentTimeMillis();
                System.out.println(" Start time - " + startTime);
            }
            string += i;
        }
        endTime = System.currentTimeMillis();
        System.out.println(" End time -   " + endTime);
        System.out.println(" Difference - " + (endTime - startTime));
        System.out.println(" Text: " + string);
    }

    private static void testStringBuffer() {
        long startTime = 0, endTime;
        StringBuffer stringBuffer = new StringBuffer();
        System.out.println("\nStingBuffer:");
        for (int i = 0; i < 10000; i++) {
            if (i == 0) {
                startTime = System.currentTimeMillis();
                System.out.println(" Start time - " + startTime);
            }
            stringBuffer.append(i);
        }
        endTime = System.currentTimeMillis();
        System.out.println(" End time -   " + endTime);
        System.out.println(" Difference - " + (endTime - startTime));
        System.out.println(" Text: " + stringBuffer);
    }

    private static void testStringBuilder() {
        long startTime = 0, endTime;
        StringBuilder stringBuilder = new StringBuilder();
        System.out.println("\nStingBuilder:");
        for (int i = 0; i < 10000; i++) {
            if (i == 0) {
                startTime = System.currentTimeMillis();
                System.out.println(" Start time - " + startTime);
            }
            stringBuilder.append(i);
        }
        endTime = System.currentTimeMillis();
        System.out.println(" End time -   " + endTime);
        System.out.println(" Difference - " + (endTime - startTime));
        System.out.println(" Text: " + stringBuilder);
    }
}
