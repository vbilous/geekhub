package lesson2;

public class SolarBattery implements EnergyProvider{
    private int batteryCharge;
    private int maxBatteryCharge;

    SolarBattery(int batteryCharge, int maxBatteryCharge) {
        this.maxBatteryCharge = maxBatteryCharge;
        if(batteryCharge>=maxBatteryCharge){
            this.batteryCharge= maxBatteryCharge;
        } else {
            this.batteryCharge = batteryCharge;
        }
    }

    public int getMaxFuel() {
        return maxBatteryCharge;
    }

    public int getCurrentCharge() {
        return batteryCharge;
    }

    public boolean addFuel(int amountFuel) {
        if (amountFuel + batteryCharge > this.maxBatteryCharge) {
            this.batteryCharge += amountFuel;
            return true;
        } else {
            return false;
        }

    }

    public boolean burnFuel(int amountFuel) {
        if ((amountFuel > 0) && (amountFuel < batteryCharge)) {
            this.batteryCharge -= amountFuel;
            return true;
        } else {
            return false;
        }
    }
}

