package lesson2;

public class Car extends Vehicle {
    private Wheel wheel;
    private GasTank gasTank;
    private PetrolEngine petrolEngine;

    Car(int balanceFuel, int maxFuel, int maxSpeed) {
        this.wheel = new Wheel(maxSpeed);
        this.gasTank = new GasTank(balanceFuel, maxFuel);
        this.petrolEngine = new PetrolEngine(false);
    }

    public int getDirection() {
        return wheel.getDirection();
    }

    public int getSpeed() {
        return wheel.getSpeed();
    }

    public int getCurrentFuel() {
        return gasTank.getCurrentFuel();
    }

    public boolean getEngineInAction() {
        return petrolEngine.getEngineInAction();
    }

    @Override
    public void accelerate(int accelerateSpeed) {
        if (gasTank.getCurrentFuel() == 0) {
            wheel.setSpeed(0);
            petrolEngine.setEngineInAction(false);
        } else {
            wheel.addSpeed(accelerateSpeed);
            gasTank.burnFuel(1);
            petrolEngine.setEngineInAction(true);
        }
    }

    @Override
    public void brake(int reduceSpeed) {
        wheel.reduceSpeed(reduceSpeed);
        if (wheel.getSpeed() == 0) {
            petrolEngine.setEngineInAction(false);
            wheel.setDirection(0);
        }
    }

    @Override
    public void turn(int direction) {
        wheel.setDirection(direction);
        wheel.reduceSpeed(1);//when the actionWithCar turns, its speed decreases.
    }
}
