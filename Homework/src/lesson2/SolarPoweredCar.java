package lesson2;


public class SolarPoweredCar extends Vehicle {
    private int batteryCharge;
    private int maxBatteryCharge;
    private Wheel wheel;
    private SolarBattery solarBattery;
    private ElectricEngine electricEngine;

    SolarPoweredCar(int batteryCharge, int maxBatteryCharge, int maxSpeed) {
        this.wheel = new Wheel(maxSpeed);
        this.solarBattery = new SolarBattery(batteryCharge, maxBatteryCharge);
        this.electricEngine = new ElectricEngine(false);
    }

    public int getDirection() {
        return wheel.getDirection();
    }

    public int getCurrentCharge() {
        return solarBattery.getCurrentCharge();
    }

    public boolean getEngineInAction() {
        return electricEngine.getEngineInAction();
    }

    public int getSpeed() {
        return wheel.getSpeed();
    }

    @Override
    public void turn(int direction) {
        wheel.setDirection(direction);
        wheel.reduceSpeed(1);//when the actionWithCar turns, its speed decreases.
    }

    @Override
    public void brake(int reduceSpeed) {
        wheel.reduceSpeed(reduceSpeed);
        if (wheel.getSpeed() == 0) {
            electricEngine.setEngineInAction(false);
            wheel.setDirection(0);
        }
    }

    @Override
    public void accelerate(int accelerateSpeed) {
        if (solarBattery.getCurrentCharge() == 0) {
            wheel.setSpeed(0);
            electricEngine.setEngineInAction(false);
        } else {
            wheel.addSpeed(accelerateSpeed);
            solarBattery.burnFuel(1);
            electricEngine.setEngineInAction(true);
        }
    }
}
