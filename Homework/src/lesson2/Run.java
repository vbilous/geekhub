package lesson2;

import java.util.Scanner;

public class Run {
    private static String selectedVehicle;
    private static Vehicle vehicle;
    private static Scanner input;
    private static int balanceFuel, maxFuel, maxSpeed;

    public static void main(String[] args) {
        chooseVehicle();
        while (true) {
            actionWithVehicle(chooseAction());
        }
    }

    private static int chooseAction() {
        System.out.print("\n1 - accelerate\n2 - brake\n3 - turn\n4 - change vehicle\n5 - exit\n Choose action: ");
        input = new Scanner(System.in);
        return input.nextInt();
    }

    private static void actionWithVehicle(int actionNumber) {
        switch (actionNumber) {
            case 1:
                System.out.print("  Enter accelerate speed: ");
                input = new Scanner(System.in);
                vehicle.accelerate(input.nextInt());
                showCurrentState();
                break;
            case 2:
                System.out.print("  Enter speed you want reduce: ");
                input = new Scanner(System.in);
                vehicle.brake(input.nextInt());
                showCurrentState();
                break;
            case 3:
                System.out.println("   1 - right");
                System.out.println("  -1 - left");
                System.out.println("   0 - straight");
                System.out.print("    Enter the direction you want to rotate: ");
                input = new Scanner(System.in);
                vehicle.turn(input.nextInt());
                showCurrentState();
                break;
            case 4:
                chooseVehicle();
                break;
            case 5:
                System.exit(0);
        }
    }

    private static void showCurrentState() {
        System.out.print("   Your current fuel (charge) - " + vehicle.getCurrentFuel());
        System.out.print("\n   Your speed - " + vehicle.getSpeed());
        switch (vehicle.getDirection()) {
            case 1:
                System.out.print("\n   Your direction - right");
                break;
            case -1:
                System.out.print("\n   Your direction - left");
                break;
            case 0:
                System.out.print("\n   Your direction - straight");
                break;
        }
        if (vehicle.getEngineInAction()) {
            System.out.print("\n   Your engine in action");
        } else {
            System.out.print("\n   Your engine is stopped");
        }
    }


    private static void chooseVehicle() {
        System.out.print("\n 1 - Car\n 2 - SolarPowerCar\n 3 - Boat \n Choose vehicle: ");
        input = new Scanner(System.in);
        selectedVehicle = input.nextLine();

        switch (Integer.parseInt(selectedVehicle)) {
            case 1: {
                enterInformationVehicle();
                vehicle = new Car(balanceFuel, maxFuel, maxSpeed);
                break;
            }
            case 2: {
                enterInformationVehicle();
                vehicle = new SolarPoweredCar(balanceFuel, maxFuel, maxSpeed);
                break;
            }
            case 3: {
                enterInformationVehicle();
                vehicle = new Boat(balanceFuel, maxFuel, maxSpeed);
                break;
            }
        }

        showCurrentState();
    }

    private static void enterInformationVehicle() {
        System.out.print("  Enter max size of the fuel tank (battery): ");
        input = new Scanner(System.in);
        maxFuel = input.nextInt();

        System.out.print("  Enter current amount of fuel (charge): ");
        input = new Scanner(System.in);
        balanceFuel = input.nextInt();

        System.out.print("  Enter maximum speed: ");
        input = new Scanner(System.in);
        maxSpeed = input.nextInt();
    }
}