package lesson7.storage;

import lesson7.objects.Cat;
import lesson7.objects.Entity;
import lesson7.objects.Ignore;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.security.Key;
import java.sql.*;
import java.util.*;

/**
 * Implementation of {@link lesson7.storage.Storage} that uses database as a storage for objects.
 * It uses simple object type names to define target table to save the object.
 * It uses reflection to access objects fields and retrieve data to map to database tables.
 * As an identifier it uses field id of {@link lesson7.objects.Entity} class.
 * Could be created only with {@link java.sql.Connection} specified.
 */
public class DatabaseStorage implements Storage {
    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws Exception {
        String sql = "SELECT * FROM " + clazz.getSimpleName() + " WHERE id = " + id;
        try (Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result.isEmpty() ? null : result.get(0);
        }
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws Exception {
        String sql = "SELECT * FROM " + clazz.getSimpleName();
        try (Statement statement = connection.createStatement()) {
            return extractResult(clazz, statement.executeQuery(sql));
        }
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws Exception {
        String sql = "DELETE FROM " + entity.getClass().getSimpleName() + " WHERE ID = " + entity.getId();
        try (Statement statement = connection.createStatement()) {
            return statement.executeUpdate(sql) == 0 ? false : true;
        }
    }

    @Override
    public <T extends Entity> void save(T entity) throws Exception {
        Map<String, Object> data = prepareEntity(entity);

        if (entity.isNew()) {
            insertData(entity, data);
        } else {
            updateData(entity, data);
        }
    }

    private <T extends Entity> void updateData(T entity, Map<String, Object> data) throws SQLException {
        String sql;
        sql = "UPDATE " + entity.getClass().getSimpleName() + " SET ";
        int counter = 0;
        for (Map.Entry<String, Object> entry : data.entrySet()) {
            counter++;
            sql += entry.getKey() + "='" + entry.getValue() + "'";
            if (counter < data.size()) {
                sql += ", ";
            }
        }
        sql += " WHERE ID = " + entity.getId();
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        }
    }

    private <T extends Entity> void insertData(T entity, Map<String, Object> data) throws Exception {
        String sql;
        StringBuilder fieldsValues = new StringBuilder();
        int i = 0;
        for (Object o : data.values()) {
            i++;
            if (o == null) {
                fieldsValues.append(o);
            } else {
                fieldsValues.append("'" + o + "'");
            }
            if (i < data.size()) {
                fieldsValues.append(", ");
            }
        }

        sql = "INSERT INTO " + entity.getClass().getSimpleName() + " (" + data.keySet().toString() +
                ") VALUES ( " + fieldsValues + " )";
        sql = sql.replace("[", "");
        sql = sql.replace("]", "");

        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
            ResultSet result = statement.getGeneratedKeys();
            result.first();
            entity.setId(result.getInt(1));
        }
    }

    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws Exception {
        Map<String, Object> map = new HashMap<>();

        for (Field field : entity.getClass().getDeclaredFields()) {
            if (!field.isAnnotationPresent(Ignore.class)) {
                Method getter = new PropertyDescriptor(field.getName(), entity.getClass()).getReadMethod();
                map.put(field.getName(), getter.invoke(entity));
            }
        }
        return map;
    }

    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultset) throws Exception {
        List<T> list = new ArrayList<>();
        while (resultset.next()) {
            T o = clazz.newInstance();
            o.setId(resultset.getInt("id"));
            for (Field field : o.getClass().getDeclaredFields()) {
                if (field.isAnnotationPresent(Ignore.class)) continue;
                field.setAccessible(true);
                field.set(o, resultset.getObject(field.getName()));
            }
            list.add(o);
        }
        return list;
    }
}
